# README #

### 토크나이저에 사용하는 사전 ###
#### dic_utils.py (파일 경로 확인 필요) ####
* 브랜드 사전
* 카테고리 사전
* 셀러 사전
* 유저 쿼리 빈도 사전
* 단어 빈도 사전
#### tr_utils.py (파일 경로 확인 필요) ####
* 단어 번역 사전

### USAGE ###

```
#!python
from Tokenizer import Tokenizer
t = Tokenizer()
print t.tokenize('바보사랑 해피슈가 겨울왕국스텐보조젓가락스푼세트 ED 57')
t.update() # 번역 사전 업데이트
```

#### options ####
* lim_prob = 0.01 (default) 이 확률 값 보다 낮을 경우 helper의 결과를 이용
* var = 0 (default) 구글 번역결과로 얻은 토큰 갯수에 +-var 하여 가장 높은 확률을 갖는 조합 선택
* helper = 'regacy' (default) -> ('twitter', 'kkma', 'trie', 'crf' 사용 가능)

### Evaluation ###

```
#!python
src_file = 'eval/pre_test.txt'
dest_file = 'eval/eval_test.txt'

t.tokenize_file(src_file, dest_file) # 파일 토크나이즈 후 자동으로 번역사전 업데이트 됨

from eval.eval_tokenizer import evaluate
evaluate(test=dest_file)
```

### TODO ###
* 토크나이징 조합에 대한 빈도수 이용 -> 예) '면 백프로', '면백 프로'