import tokenize_utils as tu
import codecs

input_path = 'eval/test.txt'
output_path = 'eval/pre_test.txt'

with codecs.open(output_path, 'w') as dest, codecs.open(input_path, 'rt') as f:
    cnt = 1
    for line in f:
        if type(line) == unicode:
            line = line.encode('utf8')

        line = tu.replace_specific_token(line)
        line = tu.replace_symbols(line)
        line = tu.tokenize_percent(line)
        line = tu.tokenize_mixed_word(line)
        line = tu.tokenize_mixed_word2(line)
        line = tu.tokenize_chinese(line)
        line = tu.tokenize_dashed_word(line)
        line = tu.replace_whitespaces(line)
        newline = ''
        for tok in line.split(' '):
            newline += tu.tokenize_size_spec(tok)
            newline += ' '
        # line = tu.tokenize_size_spec(line)
        dest.write(newline.strip()+"\n")
        cnt += 1
        if cnt%10000 == 0:
            print cnt