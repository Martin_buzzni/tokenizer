# -*- coding: utf-8 -*-
import marisa_trie
import codecs
import pickle

def save_obj(obj, name ):
    with open('obj/'+ name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name):
    with open('obj/' + name + '.pkl', 'rb') as f:
        return pickle.load(f)


def build_dic(filepath, dic={}):
    with codecs.open(filepath, 'rt', encoding='utf8', errors='replace') as f:
        cnt = 0
        for line in f:
            #if cnt > 10 : break
            if type(line) == unicode:
                line = line.encode('utf8')
            tokens = line.split(' ')

            for token in tokens:
                key = token.replace(' ', '').replace('\n','')
                # print key
                if key == '' : continue
                if type(key)==str:
                    key = key.decode('utf8')

                if dic.has_key(key) :
                    dic[key] = (1, dic[key][1]+1)
                else :
                    dic[key] = (1,1)
            cnt += 1
    return dic

def get_prob_trie(list, trie):
    keys = []
    values = []
    total = 0
    for x in list:
        keys.append(x)
        val = trie.get(x)[0][1]
        values.append(float(val))
        total += val
    values = [float(x)/total for x in values]

    return zip(keys, values)


# dic = build_dic('sampled_nshop.txt')
# dic = build_dic('sampled_hsmoa.txt', dic=dic)
# dic = sorted(dic.items(), key=lambda x: x[1], reverse=True)
# save_obj(dic, 'nshop_dic')

dic = load_obj('nshop_dic')

for k,v in dic.items():
    print k,v


print dic["백"]

fmt="<LL"
# trie = marisa_trie.RecordTrie(fmt, dic)
# with open('my_trie.marisa', 'w') as f:
#     trie.write(f)

trie = marisa_trie.RecordTrie(fmt).mmap('my_trie.marisa')

key = '백화점'
if type(key) == str:
    key = key.decode('utf8')

print key, type(key)

matched_list = trie.prefixes(key)
list = get_prob_trie(matched_list, trie)
# list = sorted(list, key=lambda x: x[1], reverse=True)
for x in matched_list:
    print "\t", x, trie.get(x)
print list
#
# matched_list = trie.prefixes(u"key")
# for x in matched_list:
#     print "\t", x, trie.get(x)