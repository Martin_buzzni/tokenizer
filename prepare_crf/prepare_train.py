#coding:utf-8
def convert_to_train(fname, fout_name):
    idx = 0
    with file(fout_name,"w") as fout:
        with file(fname) as fin:
            for line in fin.xreadlines():
                try:
                    u_list = list(line.strip().decode('utf-8'))
                except Exception,e2:
                    print e2
                    continue
                for word, next_word in zip(u_list, u_list[1:] + [" "]):
                    tag = 'W'

                    if word == ' ':
                        continue
                    if next_word == ' ':
                        tag = 'S'

                    print >> fout, word.encode('utf-8'), "\t", tag
                print >>fout, ""
                idx += 1
                if idx%10000 == 0:
                    print idx

# if __name__ == "__main__":
#     import sys
#     convert_to_train(sys.argv[1],sys.argv[2])
#
#     fname = sys.argv[2]
#     data_list = []
#     with file(fname) as fin:
#         for line in fin.xreadlines():
#             data_list.append(line.strip())
#
#     train_size = int(len(data_list)*0.9)
#
#     with file(fname+".train","w") as fout:
#         for line in data_list[:train_size]:
#             print >> fout, line
#
#     with file(fname + ".test", "w") as fout:
#         for line in data_list[train_size:]:
#             print >> fout, line

fin = '/Users/martin/workspace/tokenizer/eval/crf_model/data/concat_seg_seg.txt'
fout = '/Users/martin/workspace/tokenizer/eval/crf_model/data/concat_seg_seg.feat0'
# fin = '/Users/martin/workspace/tokenizer/eval/pre_test.txt'
# fout = '/Users/martin/workspace/tokenizer/eval/pre_test.crf.feat1'

convert_to_train(fin, fout)
