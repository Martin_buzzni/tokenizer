#coding:utf-8

def tagging(txt, mode):
    u_list = list(txt.strip().decode('utf-8'))
    result = []
    for pre_word, word, next_word in zip([" "] + u_list[:-1], u_list, u_list[1:] + [" "]):
        tag = 'W'

        if pre_word == ' ':
            tag2 = 'B'
        else:
            tag2 = 'I'

        if word == ' ':
            continue
        if next_word == ' ':
            tag = 'S'

        if mode == "train":
            result.append("".join([word.encode('utf8'), "\t", tag2, "\t", tag]))
        else:
            result.append("".join([word.encode('utf8'), "\t", tag2]))

    return result

def convert_to_train(fname, fout_name, mode="train"):
    idx = 0
    with file(fout_name,"w") as fout:
        with file(fname) as fin:
            for line in fin.xreadlines():
                items = tagging(line, mode=mode)
                for item in items:
                    print >> fout, item
                print >> fout, ""
                idx += 1
                if idx%10000 == 0:
                    print idx

# u_list = list('안녕하세요 마틴 틴틴'.decode('utf8'))
# for pre_word, word, next_word in zip([" "] + u_list[:-1], u_list, u_list[1:] + [" "]):
#     tag = 'W'
#
#     if pre_word == ' ':
#         tag2 = 'B'
#     else :
#         tag2 = 'I'
#
#     if word == ' ':
#         continue
#     if next_word == ' ':
#         tag = 'S'
#
#
#     print word, tag2, tag


# if __name__ == "__main__":
#     import sys
#     convert_to_train(sys.argv[1],sys.argv[2])
#
#     fname = sys.argv[2]
#     data_list = []
#     with file(fname) as fin:
#         for line in fin.xreadlines():
#             data_list.append(line.strip())
#
#     train_size = int(len(data_list)*0.9)
#
#     with file(fname+".train","w") as fout:
#         for line in data_list[:train_size]:
#             print >> fout, line
#
#     with file(fname + ".test", "w") as fout:
#         for line in data_list[train_size:]:
#             print >> fout, line

# fname = '/Users/martin/workspace/tokenizer/eval/pre_test.txt'
# fout_name = '/Users/martin/workspace/tokenizer/eval/pre_test.crf.feat1'

fname = '/Users/martin/workspace/tokenizer/eval/pre_test.txt'
fout_name = '/Users/martin/workspace/tokenizer/eval/pre_test.crf.feat1'


convert_to_train(fname, fout_name)
