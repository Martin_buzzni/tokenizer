#coding:utf-8
from tokenizer.tr_utils import dic_dump
from tokenizer.prototyping import segment2 as segment_test
import re
def replace_whitespaces(txt):
    s_token = re.compile('\s+')
    return s_token.sub(' ', txt)
def replace_symbols(txt):
    # txt = txt.encode('utf8') if type(txt) == unicode else txt
    p_token = re.compile(u'(?P<split>[^a-zA-Z0-9가-힣\s])')
    tokenized_str = p_token.sub(' ', txt)
    tokenized_list = [ token for token in tokenized_str.split(" ")]
    return replace_whitespaces(" ".join(tokenized_list).strip())


fname1 = "eval/data/justin_test.dat"
x_text_list = []
y_list = []
img_list = []
idx = 0
with file(fname1) as fin:
    for line in fin.xreadlines():
#         print(line.strip())
        did, img, cate, name = line.strip().split(" | ")
        x_text_list.append(name)
        y_list.append(cate)
        img_list.append(img)

        idx += 1
        if idx > 100000:
            break

from collections import Counter
target_cate_set = set()
for each in Counter(y_list).most_common(10000):
    if each[1] > 400:
        target_cate_set.add(each[0])
#     print (each[0],each[1])


def nlp(data):
    try:
        if type(data)==str:
            data = data.decode('utf-8')
        data = replace_symbols(data).encode('utf8')
        return segment_test(data)
    except:
        print 'error at:', data
        return ""


x_text_list2 = []
y_list2 = []

lenght_list = []
word_lenght_list = []

cate_ct_dict = {}
count = 0
for x, y in zip(x_text_list, y_list):
    if y not in target_cate_set:
        continue
    if not y in cate_ct_dict:
        cate_ct_dict[y] = 0
    if cate_ct_dict[y] > 400:
        continue

    count += 1
    if count < 19000:
        continue

    cate_ct_dict[y]+=1
    x_text_list2.append(nlp(x))

    y_list2.append(y)
    if count % 1000 == 0:
        print count
        dic_dump()

dic_dump()