# -*- coding: utf-8 -*-
from sklearn.externals import joblib
word_ct_dict = joblib.load("word_ct_dict.dat")

def getProb_cntDic(list, cntDic):
    keys = []
    cnts = []
    total = 0
    for key in list:
        try:
            keys.append(key)
            cnts.append(cntDic[key])
        except:
            continue
    total = sum(cnts)
    probs = [float(x)/total for x in cnts]
    return zip(keys, probs, cnts)

keys = ['신', '신세', '신세계', '신세계백화점']

print getProb_cntDic(['신', '신세', '신세계', '신세계백화점'], word_ct_dict)
print getProb_cntDic(['래', '래쉬', '래쉬가', '래쉬가드'], word_ct_dict)