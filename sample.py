# -*- coding: utf-8 -*-

from os import listdir
from os.path import isfile, join, exists
import re
import random
import codecs


def tokenize_name(txt):
    if type(txt) == unicode:
        txt = txt.encode('utf8')
    p_token = re.compile('(?P<split>]|\[|\]|\(|\)|\{|\}|,|\\|&|\*|~|~|-|_|\.|;|&|!|<|>|:|/|\+|\||◆|◀|♡|♥|★|☆|】|【|▶)')
    tokenized_str = p_token.sub(' ', txt)
    tokenized_list = [ token for token in tokenized_str.split(" ") if len(token) >=3 ]
    return " ".join(tokenized_list).decode('utf8')

# mypath = './nshop'
mypath = './hsmoa'


def sample_title_from_nshop(path, dest):
    percentage = 0.5
    cnt = 0
    with codecs.open(path, 'rt', encoding='utf8', errors='replace') as f:
        for line in f:
            if type(line) == unicode:
                line = line.encode('utf8')
            list = line.split('\t')
            if random.random() > percentage or len(list) < 3: continue
            dest.write(tokenize_name(list[2])+'\n')
            cnt += 1

def sample_title_from_hsmoa(path, dest):
    percentage = 0.01
    cnt = 0
    with codecs.open(path, 'rt', encoding='utf8', errors='replace') as f:
        for line in f:
            if type(line) == unicode:
                line = line.encode('utf8')
            dest.write(tokenize_name(line)+'\n')
            cnt += 1

def sample(foldername, sampled_file_path, elseif=None):
    onlyfiles = [f for f in listdir(foldername) if isfile(join(foldername, f))]
    with codecs.open(sampled_file_path, 'w', encoding='utf8', errors='replace') as dest:
        for fnm in onlyfiles:
            print fnm
            if foldername == 'hsmoa':
                sample_title_from_hsmoa(join(foldername, fnm), dest)
            elif foldername == 'nshop':
                sample_title_from_nshop(join(foldername, fnm), dest)

def sample_title_from(path, dest, n_data, n_sample):
    r_idx = range(n_data)
    random.shuffle(r_idx)
    r_idx = r_idx[:n_sample]
    r_idx = sorted(r_idx)

    with codecs.open(path, 'rt') as fin, codecs.open(dest, 'w') as fout:
        lines = fin.readlines()
        for idx in r_idx:
            fout.write(lines[idx])


# sample('hsmoa', 'sampled_hsmoa.txt')
# sample('nshop', 'sampled_nshop.txt')

fin = '/Users/martin/workspace/tokenizer/eval/data/test_gt_refined.txt'
fout = '/Users/martin/workspace/tokenizer/eval/data/test_gt_sampled.txt'

sample_title_from(fin, fout, 26969, 1000)


