# -*- coding: utf-8 -*-
import tokenize_utils as tu
import dic_utils as du
from tr_utils import translate

from konlpy.utils import pprint


# Parameters
MINIMUM_PROB = 1e-4 # 만약 확률이 0일 경우 부여하는 값, 최소 확률 및 최소 가중치
LIMIT_PROB = 0.4 # 이 확률 값 보다 낮을 경우 버즈니 형태소 분석기릐 결과를 이용

import pprint
class MyPrettyPrinter(pprint.PrettyPrinter):
	def format(self, _object, context, maxlevels, level):
		if isinstance(_object, unicode):
			return "'%s'" % _object.encode('utf8'), True, False
		elif isinstance(_object, str):
			_object = unicode(_object,'utf8')
			return "'%s'" % _object.encode('utf8'), True, False
		return pprint.PrettyPrinter.format(self, _object, context, maxlevels, level)

printer = MyPrettyPrinter()

def frq_items_for_pairs(word, pairs, dic):
    result = []
    for pair in pairs:
        keys = []
        cnts = []

        for key in pair:
            try :
                item = dic[tu.encode(key)]
            except :
                item = 0
                # continue

            keys.append(key)
            cnt = item

            keylen = len(tu.decode(key))
            if keylen == 1 : cnt = int(cnt/10.0) # TODO : Parameterize


            cnts.append(cnt)

        if not len(keys) == len(pair):
            continue

        new_item = zip(keys, cnts)
        result.append(new_item)
    return result

def separate_combination(word, sep):
    result = []
    if type(word) == str:
        word = word.decode('utf8')
    length = len(word)
    comb_list = []
    def separation(_len, sep, c, d=1):
        if sep == 1:
            c[d-1] = _len
            comb_list.append(c[:])
            return
        for i in range(_len-sep+1):
            n = i+1
            c[d-1] = n
            separation(_len-n, sep-1, c, d+1)
    separation(length, sep, [0] * sep)
    for item in comb_list:
        s_i = 0
        _l = []

        for i in range(sep):
            idx = item[i]
            _l.append(word[s_i:s_i+idx])
            s_i += idx
        result.append(_l)
    # result.append([word] * sep)
    # check keyword
    return result, sep

def gen_prob_dic(list_items):
    if len(list_items) == 0:
        return {}

    dics = []
    totals = []
    n_items = len(list_items[0])
    for i in range(n_items):
        dic = {}
        total = 0
        for items in list_items:
            n_items = len(items)
            if not items[i][0] in dic:
                dic[items[i][0]] = items[i][1]
                total += items[i][1]
        dics.append(dic)
        totals.append(total)

    probs = {}

    for i in range(len(dics)):
        for k,v in dics[i].items():
            if totals[i] == 0:
                probs[k] = 0
            else:
                probs[k] = float(v)/totals[i]
    return probs

def calc_prob_for_pair(list_comb, prob_dic):
    # for k,v in prob_dic.items():
    #     print k,v

    result = []
    for comb in list_comb:
        _keys = []
        _cnts = []
        prob = 1
        # prob = 0
        for _key, _cnt in comb:
            _prob = prob_dic[_key]
            _prob = MINIMUM_PROB if _prob == 0 else _prob

            prob *= _prob

            _keys.append(_key)
            _cnts.append(_cnt)
        item = (",".join(_keys), prob, sum(_cnts))
        result.append(item)
    return result

def calc_prob_for(items, dic):
    for item in items:
        key = item[0]
        if not key in dic:
            dic[key] = {
                "prob": item[1],
                "cnt": item[2]
            }
        else:
            dic[key]["prob"] += item[1]
            dic[key]["cnt"] += item[2]

def get_result(list_items, debug=False):
    info = {}
    for items in list_items:
        calc_prob_for(items, info)

    # for k,v in info.items():
    #     print k,v

    weights = [info.get(key)['cnt'] for key in info]
    total = sum(weights)

    # FIXME: 딕셔너리를 for loop 으로 순회 할 때, 그 차례가 매번 일정한지 모르겠음
    # weights = [float(w)/total for w in weights]

    # 가중치 조정
    max_prob_el = None
    max_prob = LIMIT_PROB
    for key in info:
        el = info.get(key)

        weight = 0
        if total > 0:
            weight = float(el.get('cnt'))/total

        if debug: print key, 'weight:', w

        weight = MINIMUM_PROB if weight == 0 else weight

        prob = el['prob'] * weight
        if prob > max_prob:
            max_prob = prob
            max_prob_el = (key, el)

    return max_prob_el

# 몇 개로 쪼개져야 하는지
def sep_count(word):
    to_en, to_ko = translate(word)

    if to_en is None or to_ko is None:
        return -1

    if type(to_ko) == list:
        to_ko_item = [x for x in to_ko if x[0] == word]
        if len(to_ko_item) == 0:
        # if len(to_ko) == 0:
            to_ko = word
        else:
            to_ko = to_ko_item[0][1]
            # to_ko = to_ko[0][1]

    to_en = tu.remove_article(to_en)

    cnt1 = len(to_en.split(' '))
    cnt2 = len(to_ko.split(' '))

    if not cnt1 == cnt2:
        return -1

    sep_cnt = min(cnt1, cnt2)

    return sep_cnt

def choose_one(keywords, dic):
    cnts = []

    for key in keywords:
        try:
            cnt = dic[key]
        except:
            cnt = 0
        cnts.append(cnt)

    index = cnts.index(max(cnts))
    return keywords[index]


def find_keywords(txt, dic, w_start=2, stride=1, checker=None):
    txt = tu.decode(txt)
    n_txt = len(txt)
    keywords = []

    for w_size in range(n_txt+1)[w_start:]:
        fr = 0
        to = fr + w_size
        while to <= n_txt:
            word = txt[fr:to]
            word = word.encode('utf8')

            if checker == None:
                if word in dic:
                    keywords.append(word)
            else:
                if checker(word):
                    keywords.append(word)

            fr = fr + stride
            to = fr + w_size
    return keywords

def sort_by_frq(keys, dic):
    result = []
    for key in keys:
        result.append((key, dic[key]))
    result = sorted(result, key = lambda x : x[1], reverse=True )
    result = [x[0] for x in result ]
    return result

from konlpy.tag import Twitter, Kkma
kkma = Twitter()
def nlp_segment(word):
    if type(word) == str: word = word.decode('utf8')
    return ' '.join(kkma.morphs(word))

# from datalabs.nlp.trie_tokenizer import TrieSeg
# triseg = TrieSeg(debug=False)
def trie_segment(word):
    seg, knowldege_info = triseg.segment(word, min_tok_len=4, filter_st=False, digit_norm=False)
    return seg

from mining.nlp.SegmentTagger import SegmentTagger
segmentor = SegmentTagger(model_file='/Users/martin/workspace/tokenizer/eval/crf_model/crf_model_20170223')
def crf_segment(word):
    return segmentor.segment(word)

from datalabs.ryan.buzzni.apis.nlp_api import segment as regacy_segment
def __segment(word, sep, debug=False, helper=regacy_segment):

    if sep <= 1 or sep >= len(tu.decode(word)):
        return None

    if debug:
        print 'sep count:', sep

    pairs, sep = separate_combination(word, sep)

    l1 = frq_items_for_pairs(word, pairs, du.FRQ)
    l2 = frq_items_for_pairs(word, pairs, du.FRQ2)
    # l3 = frq_items_for_pairs(word, pairs, du.FRQ3)

    items1 = calc_prob_for_pair(l1, gen_prob_dic(l1))
    items2 = calc_prob_for_pair(l2, gen_prob_dic(l2))
    # items3 = calc_prob_for_pair(l3, gen_prob_dic(l3))

    list_items = [items1, items2]

    if sum([len(x) for x in list_items]) == 0:
        return helper(word)

    segmented = get_result(list_items)

    if debug:
        print ""

        # print '--l1'
        # printer.pprint(l1)
        # print '--l2'
        # printer.pprint(l2)
        #
        # print '--items1'
        # printer.pprint(items1)
        # print '--items2'
        # printer.pprint(items2)

        print '--segmented'
        printer.pprint(segmented)

    return segmented



def _segment(word, debug=False, helper=regacy_segment):
    sep = sep_count(word)

    # decoded = tu.decode(word)
    # if sep >= len(decoded):
    #     if debug: print 'sep cnt == word len'
    #     return ' '.join(decoded)

    if sep == -1:
        if debug: print word, "'s sep count is -1"
        return helper(word)

    if sep == 1:
        if debug : print word, "'s sep count is 1"
        return word

    items = []
    if len(word.decode('utf8')) > 20 :
        return helper(word)
    else:
        var = 0 # TODO: Parameterize
    for i in range(-var, var+1):
        items.append(__segment(word, sep+i, debug=debug, helper=helper))

    if debug:
        for item in items:
            printer.pprint(item)

    cnts = []
    probs = []
    for item in items:
        if item is None:
            cnts.append(0)
            probs.append(0)
        else:
            cnts.append(item[1]['cnt'])
            probs.append(item[1]['prob'])


    total = sum(cnts)
    segmented = None

    if total > 0:
        weights = [float(x)/total for x in cnts]

        for i in range(len(probs)):
            weight = weights[i]
            weight = MINIMUM_PROB if weight == 0 else weight
            probs[i] = probs[i] * weight

        index = probs.index(max(probs))

        segmented = items[index]

    # segmented = __segment(word, sep, debug=debug, helper=helper)

    if segmented is None:
        if debug: print word, "'s segmented result is None"
        return helper(word)
    else :
        return " ".join(segmented[0].split(','))

    return result

def segment_with_keyword(word, keyword, debug=False):
    word = word.replace(keyword, ' ' + keyword + ' ').strip()
    result = ''
    for tok in word.split(' '):
        segmented = segment(tok, debug=debug)
        if type(segmented) == unicode:
            segmented = segmented.encode('utf8')
        result += segmented
        result += " "
    return result.strip()

def segment(word, debug=False):
    if debug: print '****', word, '****'
    key = tu.decode(word)

    if tu.is_unit(word):
        if debug: print word, "is a unit"
        return tu.tokenize_unit(word)

    if len(key)==1 or tu.is_english_word(word) or tu.is_model_name(word) or word.isdigit():
        if debug : print "preproc-1"
        return word

    if tu.is_multiple_unit(word):
        if debug: print word, "is a multiple unit"
        return tu.tokenize_multiple_unit(word)

    if tu.contains_number_in_korean(word):
        if debug: print word, "contains a number among korean"
        return regacy_segment(word)

    if du.is_brand(word):
        if debug: print word, "is a brand"
        return word

    # if is_cate(word):
    #     # cates = find_keywords(word, du.CATE)
    #     # cate = choose_one(cates, du.FRQ)
    #     # if cate == word:
    #     if debug: print word, "is a category"
    #     return word

    if du.is_seller(word):
        if debug: print word, "is a seller"
        return word

    # sellers = find_keywords(word, du.SELLER, checker=du.is_seller)
    # sellers = sort_by_frq(sellers, du.SELLER)
    sellers = find_keywords(word, du.SELLER)
    if len(sellers) > 0 and not sellers[0] == word :
        seller = sellers[0]
        if debug : print word, 'contains a seller:', seller
        return segment_with_keyword(word, seller, debug=debug)

    if len(key) > 10 :
        cates = find_keywords(word, du.CATE)
        if len(cates) > 0 :
            cate = choose_one(cates, du.FRQ)
            if debug: print word, 'contains a category:', cate
            return segment_with_keyword(word, cate, debug=debug)

    # return crf_segment(word)
    # return trie_segment(word)
    return _segment(word, debug=debug)
    # return nlp_segment(word)
    # return regacy_segment(word)
    # return word

## USAGE
# printer.pprint( segment('하와이언디잉티셔츠') )
# printer.pprint( segment('T자수포인트데님SHLP5TSH01') )
# printer.pprint( segment('린넨해지프리트셔츠') )
# printer.pprint( segment('T아가타숏상하복') )
# printer.pprint( segment('머스트다캣탑') )
# printer.pprint( segment('현대백화점7관') )
# printer.pprint( segment('아동') )
# printer.pprint( segment('여아국내생산편안한트레이닝스커트레깅스체크카라남방') )
# printer.pprint( segment('라운드티토들러부터주니어용긴팔라운드티') )
# printer.pprint( segment('블루퍼플케비노') )


# printer.pprint( segment('파워서플라이', debug=True) ) #  power supply -> 전원 공급 장치
# printer.pprint( segment('에그링', debug=True) ) # Egring으로 번역
# printer.pprint( segment('매직베드', debug=True) ) # 브랜드
# printer.pprint( segment('면백프로', debug=True) ) # (면백 프로)가 prd dic에 더 많음
# printer.pprint( segment('화이트라벨', debug=True) ) # 브랜드
# printer.pprint( segment('기모면바지', debug=True) ) # 그냥 바지로 번역됨
# printer.pprint( segment('상황버섯추출액', debug=True) ) # 상황버섯 추출액
# printer.pprint( segment('폴리체크자가드남성삼각팬티', debug=True) ) # 토크나이징 개수 문제 --> 폴리체크자가드 남성 삼각 팬티
# printer.pprint( segment('키홀더', debug=True) ) #
# printer.pprint( segment('소가죽', debug=True) ) #
# printer.pprint( segment('라이프스타일', debug=True) ) # 브랜드
# printer.pprint( segment('메세지택', debug=True) ) # 유저 쿼리에 없음
# printer.pprint( segment('콧수염호피치랭스', debug=True) ) # 번역결과 확인해보기
# printer.pprint( segment('골드라벨', debug=True) ) # 브랜드
# printer.pprint( segment('잔디밭', debug=True) ) # sep 1
# printer.pprint( segment('로브너클라리넷리가쳐', debug=True) )
# printer.pprint( segment('루즈핏', debug=True) ) # 룰렛으로 번역됨
# printer.pprint( segment('레몬그라스', debug=True) ) # 브랜드
# printer.pprint( segment('마이크로폴리언트', debug=True) ) #
# printer.pprint( segment('미용실아이콘원형', debug=True) ) # (미용실 아이 콘 원형)이 더 유리하게 선택됨
# printer.pprint( segment('등판로고포인트롱항공', debug=True) ) # sep -1 -->
# printer.pprint( segment('남자빅사이즈야자수반팔면티', debug=True) ) #
# printer.pprint( segment('10인310cm', debug=True) ) #
# printer.pprint( segment('덕다운', debug=True) ) # sep -1 -->
# printer.pprint( segment('여름신상', debug=True) ) # 섬머로 번역됨
# printer.pprint( segment('국내생산', debug=True) ) # (국 내생산)이 dic1,2에 빈도수 더 많음
# printer.pprint( segment('올리브그린', debug=True) ) # 브랜드
# printer.pprint( segment('원형찬기', debug=True) ) # sep 1
# printer.pprint( segment('특가바보사랑해외여행', debug=True) ) # category search test
# printer.pprint( segment('물기제거', debug=True) ) # 한글자 영어로 번역됨
# printer.pprint( segment('털부츠', debug=True) ) # 털부츠 -> boots로 번역됨
# printer.pprint( segment('삼파장일반전구포함', debug=True) ) # sep -1
# printer.pprint( segment('태양광발전기', debug=True) ) # (태양광 발 전기) 더 유리
# printer.pprint( segment('오버핏', debug=True) ) # 확률이 낮음
# printer.pprint( segment('11월', debug=True) )
# printer.pprint( segment('곰돌여아', debug=True) )
# printer.pprint( segment('16종세트', debug=True) )
# printer.pprint( segment('메탈선반750', debug=True) )
# printer.pprint( segment('3중안전장치', debug=True) )
# printer.pprint( segment('10인310cm', debug=True) )
printer.pprint( segment('30hv3000식당공장포충기모기퇴치날벌레퇴치포충등', debug=True) )
# print crf_segment('바보사랑북유럽스타일')

pairs, sep = separate_combination('30hv3000식당공장포충기모기퇴치날벌레퇴치포충등', 8)