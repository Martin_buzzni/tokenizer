import utils.src.timeutil as timeutil
import utils.src.dbutil as dbutil
import utils.src.gvalue as gvalue
import tokenize_utils as tu

def str2uni(txt):
    return tu.decode(txt)

def connect_db():
    try:
        global shop_db
        shop_db = dbutil.conn(gvalue._SHOPPINGDB_SLAVE_URL, db='shopping', pwd=gvalue.AWSDB_PWD)
    except Exception as e:
        print "db connection fail:",e

def load_attr_dict_from_db(table_name="attr_keyword"):
    try:
        global shop_db
        attr_dict = {}
        _sql = "select attr_val,attr_type from %s" % (table_name)
        _rslt = shop_db.execute(_sql)

        if _rslt:
            for item in _rslt:
                attr_val = item[0].replace(" ", "").upper()
                attr_type = item[1].replace(" ", "").upper()
                if type(attr_val) == unicode:
                    attr_val = attr_val.encode('utf8')

                if type(attr_type) == unicode:
                    attr_type = attr_type.encode('utf8')

                if attr_val not in attr_dict:
                    attr_dict[attr_val] = attr_type

        print "attr dic:", len(attr_dict)
        return attr_dict
    except Exception as e:
        print "load_attr_dict_from_db fail:", e
        return {}


def load_catename_from_db(table_name="buzzni_catekeyword"):
    try:
        global shop_db
        catename_dict = {}
        _sql = "select keyword,root_cate_name from %s" % (table_name)
        _rslt = shop_db.execute(_sql)

        if _rslt:
            for item in _rslt:
                cate_name_str = item[0].replace(" ", "").upper()
                if type(cate_name_str) == unicode:
                    cate_name_str = cate_name_str.encode('utf8')

                ncate1 = item[1].replace(" ", "").upper()
                if type(ncate1) == unicode:
                    ncate1 = ncate1.encode('utf8')

                if cate_name_str not in catename_dict:
                    catename_dict[cate_name_str] = ncate1

        print "catename dic:", len(catename_dict)
        return catename_dict
    except Exception as e:
        print "load_catename_from_db fail:", e
        return {}


def load_brand_dict_from_db(table_name="buzzni_brand", status=-1):
    try:
        global shop_db
        brand_dic = {}
        if status == 0 or status == 1 or status == 2:
            _sql = "select parent_brand,brand,cate1,brand_count from %s where status=%d " % (table_name, status)
        else:
            _sql = "select parent_brand,brand,cate1,brand_count from %s" % (table_name)

        _rslt = shop_db.execute(_sql)

        if _rslt:
            for item in _rslt:
                parent_brand = item[0].replace(" ", "").upper()
                brand = item[1].replace(" ", "").upper()
                cate1 = item[2].replace(" ", "").upper()
                brand_count = item[3]

                if brand not in brand_dic:
                    info = {'p_brand': parent_brand,
                            'brand': brand,
                            'cnt': brand_count,
                            'cate': {cate1: brand_count}
                            }
                    brand_dic[brand] = info
                else:
                    info = brand_dic[brand]
                    info['cnt'] += brand_count

                    if cate1 in info['cate']:
                        info['cate'][cate1] += brand_count
                    else:
                        info['cate'][cate1] = brand_count

        print "brand dic:", len(brand_dic)
        return brand_dic
    except Exception as e:
        print "load_brand_dict_from_db fail:", e
        return {}


def preprocessing_data(brand_dic, cate_dic, attr_dic, word_frq_list, from_idx=0, last_idx=-1):
    VAL_BRAND = 1
    VAL_CATE = 2
    VAL_ATTR = 3
    VAL_PRD = 4

    dic_word_list = []
    dic_word_list_reverse = []

    brand_word_list = []
    brand_word_list_reverse = []
    for idx, (brand, info) in enumerate(brand_dic.items()[from_idx:last_idx]):
        if len(brand) < 1:
            continue

        if is_unigram_korean_brand(brand):
            continue
        frq = info.get('cnt', 0)

        brand = str2uni(brand)
        brand_reverse = "".join(reversed(brand))

        dic_info = (brand, (VAL_BRAND, frq))
        dic_info_reverse = (brand_reverse, (VAL_BRAND, frq))

        brand_word_list.append(dic_info)
        brand_word_list_reverse.append(dic_info_reverse)

    dic_word_list += brand_word_list
    dic_word_list_reverse += brand_word_list_reverse

    print "Trie Brand dic:", len(brand_word_list), len(dic_word_list)
    print "Trie Brand dic reverse:", len(brand_word_list_reverse), len(dic_word_list_reverse)

    cate_word_list = []
    cate_word_list_reverse = []
    for idx, (cate, info) in enumerate(cate_dic.items()[from_idx:last_idx]):
        cate = str2uni(cate)
        cate_reverse = "".join(reversed(cate))

        dic_info = (cate, (VAL_CATE, 1))
        dic_info_reverse = (cate_reverse, (VAL_CATE, 1))

        cate_word_list.append(dic_info)
        cate_word_list_reverse.append(dic_info_reverse)

    dic_word_list += cate_word_list
    dic_word_list_reverse += cate_word_list_reverse
    print "Trie cate dic:", len(cate_word_list), len(dic_word_list)
    print "Trie cate dic reverse:", len(cate_word_list_reverse), len(dic_word_list_reverse)

    attr_word_list = []
    attr_word_list_reverse = []
    for idx, (attr, info) in enumerate(attr_dic.items()[from_idx:last_idx]):
        attr = str2uni(attr)
        attr_reverse = "".join(reversed(attr))

        dic_info = (attr, (VAL_ATTR, 1))
        dic_info_reverse = (attr_reverse, (VAL_ATTR, 1))
        attr_word_list.append(dic_info)
        attr_word_list_reverse.append(dic_info_reverse)

    dic_word_list += attr_word_list
    dic_word_list_reverse += attr_word_list_reverse
    print "Attr cate dic:", len(attr_word_list), len(dic_word_list)
    print "Attr cate dic reverse:", len(attr_word_list_reverse), len(dic_word_list_reverse)

    prd_word_list = []
    prd_word_list_reverse = []
    for idx, (word, frq) in enumerate(word_frq_list[from_idx:last_idx]):
        if frq < 10:
            break
        if word == 'NUMSYN':
            continue

        word = str2uni(word)
        word_reverse = "".join(reversed(word))

        dic_info = (word, (VAL_PRD, frq))
        dic_info_reverse = (word_reverse, (VAL_PRD, frq))

        prd_word_list.append(dic_info)
        prd_word_list_reverse.append(dic_info_reverse)

    dic_word_list += prd_word_list
    dic_word_list_reverse += prd_word_list_reverse
    print "Prd word dic:", len(prd_word_list), len(dic_word_list)
    print "Prd word dic reverse:", len(prd_word_list_reverse), len(dic_word_list_reverse)

    return dic_word_list, dic_word_list_reverse


def isvalid_han_syllable(ch):
    if type(ch) == str:
        ch = ch.decode('utf8')
    HAN_S = 0xAC00
    HAN_E = 0xD7AF
    code = ord(ch)
    return (code >= HAN_S and code <= HAN_E)


def is_unigram_korean_brand(brand):
    if type(brand) == str:
        brand = brand.decode('utf8')

    if len(brand) == 1 and isvalid_han_syllable(brand[0]):
        return True
    else:
        return False

connect_db()
brand_dic = load_brand_dict_from_db()
