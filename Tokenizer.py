# -*- coding: utf-8 -*-
import tokenize_utils as tu
import pprint
from tr_utils import translate, dic_dump

class MyPrettyPrinter(pprint.PrettyPrinter):
	def format(self, _object, context, maxlevels, level):
		if isinstance(_object, unicode):
			return "'%s'" % _object.encode('utf8'), True, False
		elif isinstance(_object, str):
			_object = unicode(_object,'utf8')
			return "'%s'" % _object.encode('utf8'), True, False
		return pprint.PrettyPrinter.format(self, _object, context, maxlevels, level)

printer = MyPrettyPrinter()

def get_helper(helper):
    result = None
    if helper == "legacy":
        from datalabs.ryan.buzzni.apis.nlp_api import segment
        result = segment
    elif helper == "twitter":
        from konlpy.tag import Twitter
        twitter = Twitter()
        def segment(word):
            if type(word) == str: word = word.decode('utf8')
            return ' '.join(twitter.morphs(word))
        result = segment
    elif helper == "trie":
        from datalabs.nlp.trie_tokenizer import TrieSeg
        trieseg = TrieSeg(debug=False)
        def segment(word):
            seg, knowldege_info = trieseg.segment(word, min_tok_len=4, filter_st=False, digit_norm=False)
        result = segment
    elif helper == "crf":
        from mining.nlp.SegmentTagger import SegmentTagger
        segmentor = SegmentTagger(model_file='eval/crf_model/crf_model_20170223')
        def segment(word):
            return segmentor.segment(word)
        result = segment
    elif helper == "kkma":
        from konlpy.tag import Kkma
        kkma = Kkma()
        def segment(word):
            if type(word) == str: word = word.decode('utf8')
            return ' '.join(kkma.morphs(word))
        result = segment

    return result

class Tokenizer:
    def __init__(self, helper="legacy", debug=False, min_prob=1e-4, lim_prob=0.05, discounter=1/5.0, var=0):
        self.debug = debug
        self.helper = get_helper(helper)

        # Parameters
        self.MINIMUM_PROB = min_prob # 만약 확률이 0일 경우 부여하는 값, 최소 확률 및 최소 가중치
        self.LIMIT_PROB = lim_prob # 이 확률 값 보다 낮을 경우 helper의 결과를 이용
        self.DISCOUNTER = discounter
        self.VAR_TOK_LENGTH = var

        import dic_utils as du
        self.DU = du

    def frq_items_for_pairs(self, word, pairs, dic):
        n_word = len(tu.decode(word))
        result = []
        for pair in pairs:
            keys = []
            cnts = []

            for key in pair:
                try:
                    item = dic[tu.encode(key)]
                except:
                    item = 0
                    # continue

                keys.append(key)
                cnt = item

                n_key = len(tu.decode(key))
                # TODO : parameterize
                if n_word > 3 and n_key == 1 : cnt = int(cnt * self.DISCOUNTER)

                cnts.append(cnt)

            if not len(keys) == len(pair):
                continue

            new_item = zip(keys, cnts)
            result.append(new_item)
        return result

    def separate_combination(self, word, sep):
        result = []
        if type(word) == str:
            word = word.decode('utf8')
        length = len(word)
        comb_list = []
        def separation(_len, sep, c, d=1):
            if sep == 1:
                c[d-1] = _len
                comb_list.append(c[:])
                return
            for i in range(_len-sep+1):
                n = i+1
                c[d-1] = n
                separation(_len-n, sep-1, c, d+1)
        separation(length, sep, [0] * sep)
        for item in comb_list:
            s_i = 0
            _l = []

            for i in range(sep):
                idx = item[i]
                _l.append(word[s_i:s_i+idx])
                s_i += idx
            result.append(_l)
        # result.append([word] * sep)
        # check keyword
        return result, sep


    def update(self):
        dic_dump()
        print "TR dics have been updated."

    # 몇 개로 쪼개져야 하는지
    def sep_count(self, word):
        to_en, to_ko = translate(word, debug=self.debug)

        if to_en is None or to_ko is None:
            return -1

        if type(to_ko) == list:
            to_ko_item = [x for x in to_ko if x[0] == word]
            if len(to_ko_item) == 0:
                # if len(to_ko) == 0:
                to_ko = word
            else:
                to_ko = to_ko_item[0][1]
                # to_ko = to_ko[0][1]

        to_en = tu.remove_article(to_en)

        cnt1 = len(to_en.split(' '))
        cnt2 = len(to_ko.split(' '))

        if not cnt1 == cnt2:
            return -1

        sep_cnt = min(cnt1, cnt2)

        return sep_cnt

    def sort_by_frq(self, keys, dic):
        result = []
        for key in keys:
            result.append((key, dic[key]))
        result = sorted(result, key = lambda x : x[1], reverse=True )
        result = [x[0] for x in result ]
        return result

    def calc_prob_for_pair(self, list_comb, prob_dic):
        result = []
        for comb in list_comb:
            _keys = []
            _cnts = []
            prob = 1
            # prob = 0
            for _key, _cnt in comb:
                _prob = prob_dic[_key]
                _prob = self.MINIMUM_PROB if _prob == 0 else _prob

                prob *= _prob

                _keys.append(_key)
                _cnts.append(_cnt)
            item = (",".join(_keys), prob, sum(_cnts))
            result.append(item)
        return result

    def calc_prob_for(self, items, dic):
        for item in items:
            key = item[0]
            if not key in dic:
                dic[key] = {
                    "prob": item[1],
                    "cnt": item[2]
                }
            else:
                dic[key]["prob"] += item[1]
                dic[key]["cnt"] += item[2]

    def get_result(self, list_items, debug=False):
        info = {}
        for items in list_items:
            self.calc_prob_for(items, info)

        # for k,v in info.items():
        #     print k,v

        weights = [info.get(key)['cnt'] for key in info]
        total = sum(weights)

        # FIXME: 딕셔너리를 for loop 으로 순회 할 때, 그 차례가 매번 일정한지 모르겠음
        # weights = [float(w)/total for w in weights]

        # 가중치 조정
        max_prob_el = None
        max_prob = self.LIMIT_PROB
        for key in info:
            el = info.get(key)

            weight = 0
            if total > 0:
                weight = float(el.get('cnt')) / total

            if debug: print key, 'weight:', w

            weight = self.MINIMUM_PROB if weight == 0 else weight

            prob = el['prob'] * weight
            if prob > max_prob:
                max_prob = prob
                max_prob_el = (key, el)

        return max_prob_el

    def gen_prob_dic(self, list_items):
        if len(list_items) == 0:
            return {}

        dics = []
        totals = []
        n_items = len(list_items[0])
        for i in range(n_items):
            dic = {}
            total = 0
            for items in list_items:
                n_items = len(items)
                if not items[i][0] in dic:
                    dic[items[i][0]] = items[i][1]
                    total += items[i][1]
            dics.append(dic)
            totals.append(total)

        probs = {}

        for i in range(len(dics)):
            for k, v in dics[i].items():
                if totals[i] == 0:
                    probs[k] = 0
                else:
                    probs[k] = float(v) / totals[i]
        return probs

    def __tokenize(self, word, sep):

        if sep <= 1 or sep >= len(tu.decode(word)):
            return None

        if self.debug:
            print 'sep count:', sep

        pairs, sep = self.separate_combination(word, sep)

        l1 = self.frq_items_for_pairs(word, pairs, self.DU.FRQ)
        l2 = self.frq_items_for_pairs(word, pairs, self.DU.FRQ2)

        items1 = self.calc_prob_for_pair(l1, self.gen_prob_dic(l1))
        items2 = self.calc_prob_for_pair(l2, self.gen_prob_dic(l2))

        list_items = [items1, items2]

        if sum([len(x) for x in list_items]) == 0:
            return self.helper(word)

        segmented = self.get_result(list_items)

        if self.debug:
            print ""

            print '--l1'
            printer.pprint(l1)
            print '--l2'
            printer.pprint(l2)
            #
            print '--items1'
            printer.pprint(items1)
            print '--items2'
            printer.pprint(items2)
            #
            # print '--segmented'
            # printer.pprint(segmented)

        return segmented

    def _tokenize(self, word):
        sep = self.sep_count(word)

        # decoded = tu.decode(word)
        # if sep >= len(decoded):
        #     if debug: print 'sep cnt == word len'
        #     return ' '.join(decoded)

        if sep == -1:
            if self.debug: print word, "'s sep count is -1"
            return self.helper(word)

        if sep == 1:
            if self.debug: print word, "'s sep count is 1"
            return word

        items = []

        # FIXME: 20글자 이상의 텍스트의 경우 조합가짓수를 구하는데 연산량이 매우 커짐
        if len(word.decode('utf8')) > 20:
            return self.helper(word)

        for i in range(-self.VAR_TOK_LENGTH, self.VAR_TOK_LENGTH + 1):
            items.append(self.__tokenize(word, sep + i))

        if self.debug:
            for item in items:
                printer.pprint(item)

        cnts = []
        probs = []
        for item in items:
            if item is None:
                cnts.append(0)
                probs.append(0)
            else:
                cnts.append(item[1]['cnt'])
                probs.append(item[1]['prob'])

        total = sum(cnts)
        segmented = None

        if total > 0:
            weights = [float(x) / total for x in cnts]

            for i in range(len(probs)):
                weight = weights[i]
                weight = self.MINIMUM_PROB if weight == 0 else weight
                probs[i] = probs[i] * weight

            index = probs.index(max(probs))

            segmented = items[index]

        # segmented = __segment(word, sep, debug=debug, helper=helper)

        if segmented is None:
            if self.debug: print word, "'s segmented result is None"
            return self.helper(word)
        else:
            return " ".join(segmented[0].split(','))

        return result

    def choose_one(self, keywords, dic):
        cnts = []

        for key in keywords:
            try:
                cnt = dic[key]
            except:
                cnt = 0
            cnts.append(cnt)

        index = cnts.index(max(cnts))
        return keywords[index]

    def find_keywords(self, txt, dic, w_start=2, stride=1, checker=None):
        txt = tu.decode(txt)
        n_txt = len(txt)
        keywords = []

        for w_size in range(n_txt + 1)[w_start:]:
            fr = 0
            to = fr + w_size
            while to <= n_txt:
                word = txt[fr:to]
                word = word.encode('utf8')

                if checker == None:
                    if word in dic:
                        keywords.append(word)
                else:
                    if checker(word):
                        keywords.append(word)

                fr = fr + stride
                to = fr + w_size
        return keywords

    def sort_by_frq(self, keys, dic):
        result = []
        for key in keys:
            result.append((key, dic[key]))
        result = sorted(result, key=lambda x: x[1], reverse=True)
        result = [x[0] for x in result]
        return result

    def tokenize_with_keyword(self, word, keyword):
        word = word.replace(keyword, ' ' + keyword + ' ').strip()
        result = ''
        for tok in word.split(' '):
            segmented = self.tokenize_by_word(tok)
            if type(segmented) == unicode:
                segmented = segmented.encode('utf8')
            result += segmented
            result += " "
        return result.strip()

    def tokenize_by_word(self, word):
        if self.debug: print '****', word, '****'
        key = tu.decode(word)

        if tu.is_unit(word):
            if self.debug: print word, "is a unit"
            return tu.tokenize_unit(word)

        if len(key) == 1 or tu.is_english_word(word) or tu.is_model_name(word) or tu.is_number(word):
            if self.debug: print "preproc-1"
            return word

        if tu.is_multiple_unit(word):
            if self.debug: print word, "is a multiple unit"
            return tu.tokenize_multiple_unit(word)

        if tu.contains_number_in_korean(word):
            if self.debug: print word, "contains a number among korean"
            return self.helper(word)

        if self.DU.is_brand(word):
            if self.debug: print word, "is a brand"
            return word

        # if is_cate(word):
        #     # cates = find_keywords(word, du.CATE)
        #     # cate = choose_one(cates, du.FRQ)
        #     # if cate == word:
        #     if debug: print word, "is a category"
        #     return word

        if self.DU.is_seller(word):
            if self.debug: print word, "is a seller"
            return word

        # sellers = find_keywords(word, du.SELLER, checker=du.is_seller)
        # sellers = sort_by_frq(sellers, du.SELLER)
        sellers = self.find_keywords(word, self.DU.SELLER)
        if len(sellers) > 0 and not sellers[0] == word:
            seller = sellers[0]
            if self.debug: print word, 'contains a seller:', seller
            return self.tokenize_with_keyword(word, seller)

        if len(key) > 8:
            cates = self.find_keywords(word, self.DU.CATE)
            if len(cates) > 0:
                cate = self.choose_one(cates, self.DU.FRQ)
                if self.debug: print word, 'contains a category:', cate
                return self.tokenize_with_keyword(word, cate)

        return self._tokenize(word)

    def tokenize_by_line(self, line):
        if type(line) == unicode:
            line = line.encode('utf8')

        transformed_line = ""
        for tok in line.replace('\t', ' ').split(' '):
            try:
                transformed_tok = self.tokenize_by_word(tok.strip())
            except:
                print "error at:", tok
                transformed_tok = tok

            transformed_tok = tu.encode(transformed_tok)

            transformed_line += transformed_tok
            transformed_line += " "
        transformed_line = tu.replace_whitespaces(transformed_line)
        return transformed_line.strip()

    def tokenize(self, txt):
        txt = tu.encode(txt)
        txt = tu.replace_specific_token(txt)
        txt = tu.replace_symbols(txt)
        txt = tu.tokenize_percent(txt)
        txt = tu.tokenize_mixed_word(txt)
        txt = tu.tokenize_mixed_word2(txt)
        txt = tu.tokenize_chinese(txt)
        txt = tu.tokenize_dashed_word(txt)
        txt = tu.replace_whitespaces(txt)
        newline = ''
        for tok in txt.split(' '):
            newline += tu.tokenize_size_spec(tok)
            newline += ' '
            # line = tu.tokenize_size_spec(line)
        newline = newline.strip()
        return self.tokenize_by_line(newline)

    def tokenize_file(self, src_file, dest_file):
        import codecs
        with codecs.open(dest_file, 'w') as d, codecs.open(src_file, 'rt') as f:
            cnt = 0
            for line in f:
                line = line.replace('\n', '')
                transformed_line = self.tokenize(line)
                d.write(tu.encode(transformed_line) + "\n")

                cnt += 1
                if cnt % 100 == 0:
                    print cnt
        self.update()

# t = Tokenizer(debug=True)
# t.tokenize_by_word("남성의류")
# print t.tokenize_by_word("상황버섯추출액")
# print t.tokenize_by_word("꽃무늬블라우스")
# print t.tokenize('바보사랑 해피슈가 겨울왕국스텐보조젓가락스푼세트 ED 57')
# t = Tokenizer(lim_prob=0.05)
# src_file = 'eval/pre_test.txt'
# dest_file = 'eval/eval_test.txt'
# 
# t.tokenize_file(src_file, dest_file)
# 
# from eval.eval_tokenizer import evaluate
# evaluate(test=dest_file)
