# -*- coding: utf-8 -*-
from sklearn.externals import joblib
from subprocess import Popen, PIPE
from tokenize_utils import is_english_word, is_model_name, is_number
import codecs

def gen_key(key):
    if type(key) == str:
        return key.decode('utf8')

def translate(word):
	p = Popen('/usr/local/bin/node /Users/martin/workspace/sample_title_data/test_translate.js ' + word, stdin=PIPE, stdout=PIPE,
			  stderr=PIPE, shell=True)
	output, err = p.communicate()
	rc = p.returncode
	return output

def build(filepath, to_en_dic={}, to_ko_dic={}):
    with codecs.open(filepath, 'rt', encoding='utf8', errors='replace') as f:
        cnt = 0
        for line in f:
            print str(cnt),": ",line
            if type(line) == unicode:
                line = line.encode('utf8')
            line = line.replace('\n', '')
            toks = line.split(' ')

            for tok in toks:
                if tok == '' or is_english_word(tok) or is_model_name(tok) or is_number(tok): continue

                if not to_en_dic.has_key(tok):
                    try :
                        to_en, to_ko = translate(tok).split("|")
                        tok2 = to_en
                        to_en_dic[tok] = to_en
                        if not to_ko_dic.has_key(tok2):
                            to_ko_dic.setdefault(tok2, [])
                        to_ko_dic[tok2].append((tok, to_ko))
                    except:
                        print 'error at:', tok
                        continue

            cnt += 1
            # if cnt==1000:
            #     break
    return to_en_dic, to_ko_dic

# clf = joblib.load('tr_dic.pkl')

to_en_dic, to_ko_dic = build('eval/pre_test.txt')
# to_en_dic, to_ko_dic = build('sampled_hsmoa.txt', to_en_dic=to_en_dic, to_ko_dic=to_ko_dic)

joblib.dump(to_en_dic, 'dic/to_en_dic_dev_test.pkl')
joblib.dump(to_ko_dic, 'dic/to_ko_dic_dev_test.pkl')

# test
# to_en_dic = joblib.load('to_en_dic50.pkl')
# to_ko_dic = joblib.load('to_ko_dic50.pkl')
#
# print to_en_dic["래쉬가드"]
# list = to_ko_dic["Rash Guard"]
# print 'len:', len(list)
# for i in list:
#     print '\t', i[0], i[1]