# -*- coding: utf-8 -*-
# 단어에대해 sep 갯수만큼 분할 할 수 있는 모든 경우의 리스트를 구함
def separate_combination(word, sep):
    result = []
    if type(word) == str:
        word = word.decode('utf8')
    length = len(word)
    comb_list = []
    def separation(_len, sep, c, d=1):
        if sep == 1:
            c[d-1] = _len
            comb_list.append(c[:])
            return
        for i in range(_len-sep+1):
            n = i+1
            c[d-1] = n
            separation(_len-n, sep-1, c, d+1)
    separation(length, sep, [0] * sep)
    for item in comb_list:
        s_i = 0
        _l = []

        for i in range(sep):
            idx = item[i]
            _l.append(word[s_i:s_i+idx])
            s_i += idx
        result.append(_l)
    result.append([word]*sep)
    return result

l = separate_combination("반소매망사스웨터", 3)
for item in l:
    print item[0],item[1],item[2]

import random
def is_keyword(word):
    if type(word) == unicode:
        word = word.encode('utf8')
    return word in ['반소매', '망사', '스웨터', '반', '소매', '망사스웨터']

l2 = l[:]
l3 = []
keywords = []
while len(l2) > 0:
    check = 1
    for toks in l2:
        _n = -1
        _i = -1
        for i in range(len(toks)):
            tok = toks[i]
            if is_keyword(tok): continue
            keywords.append(tok)
            _n = len(tok)
            _i = i
            break
        if _i < 0: continue
        def _filter(item):
            return not len(item[_i]) == _n
        l2 = filter(_filter, l2)
        check = 0
        break
    if check :
        break

for item in l2:
    # print item
    print item[0],item[1],item[2]

# def test_filter(item):
#     print len(item)
#
# filter(test_filter, l)

