# -*- coding: utf-8 -*-
from sklearn.externals import joblib

cate_dic = joblib.load("/Users/martin/workspace/datalabs/nlp/dict/cate.dict")
fq_dic = joblib.load("/Users/martin/workspace/datalabs/nlp/dict/prd.dict")

def find_keywords(txt, dic, w_start=2, stride=1):
    if type(txt) == str: txt = txt.decode('utf8')
    n_txt = len(txt)
    keywords = []

    for w_size in range(n_txt+1)[w_start:]:
        fr = 0
        to = fr + w_size
        while to <= n_txt:
            word = txt[fr:to]
            word = word.encode('utf8')

            if word in dic:
                keywords.append(word)

            fr = fr + stride
            to = fr + w_size
    return keywords

keywords = find_keywords('재킷', cate_dic)
for i in keywords:
    print i, fq_dic[i]