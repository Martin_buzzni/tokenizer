# -*- coding: utf-8 -*-
import re
from common import encode, decode, replace_whitespaces
# from quantulum import parser
# from utils.src.Hangul import Hangul

def replace_specific_token(txt):
    items = [('f/w', 'fw'), ('s/s', 'ss'), ('f/s', 'fs'),('F/W', 'FW'), ('S/S', 'SS'), ('F/S', 'FS')]
    for item in items:
        txt = txt.replace(item[0], item[1])
    return txt

def normalize_number(txt):
    n_token = re.compile('[0-9]+')
    return n_token.sub('NUMSYM', txt)

def replace_symbols(txt):
    txt = encode(txt)
    p_token = re.compile('(?P<split>]|\[|\]|\(|\)|\{|\}|,|\\|&|~|_|;|&|!|<|>|:|/|\+|\||◆|◀|♡|♥|ㆍ|┃|★|☆|◐|】|【|▶|ㅡ|［|］)')
    tokenized_str = p_token.sub(' ', txt)
    tokenized_list = [ token for token in tokenized_str.split(" ")]
    return " ".join(tokenized_list).decode('utf8').strip()

def is_seller(word):
    import dic_utils as du
    return du.is_seller(word)

def is_model_name(word):

    word = encode(word)
    # TODO : brand 체크..
    if is_seller(word): return False

    model_char_re = re.compile("[0-9a-zA-Z\-]{5,}$")
    return model_char_re.match(word) is not None

def is_unit(word):
    word = encode(word)
    re_items = []
    re_items.append(re.compile('^[0-9]+\.?[0-9]*[a-zA-Z]{1,2}$'))
    re_items.append(re.compile('^[0-9]+\.?[0-9]*[가-힣]{1,6}$'))

    result = False

    for regex in re_items:
        if regex.match(word) is not None:
            result = True
            break

    return result

def is_english_word(word):
    word = encode(word)
    model_char_re = re.compile("^[a-zA-Z]+$")
    return model_char_re.match(word) is not None

def is_korean_word(word):
    word = encode(word)
    model_char_re = re.compile("^[가-힣]+$")
    return model_char_re.match(word) is not None

def is_number(word):
    word = encode(word)
    model_char_re = re.compile("[0-9]*\.?[0-9]*")
    matched = model_char_re.match(word)
    if matched:
        if matched.group(0) == word:
            return True

    return False

def tokenize_chinese(txt):
    txt = decode(txt)
    rx = re.compile(u'([一-龥]+)')
    txt = rx.sub(' \g<1> ', decode(txt)).strip()
    return encode(txt)

def tokenize_mixed_word_reverse(word):
    model_char_re = re.compile("([a-zA-Z]{1})([가-힣]{1})")
    word = model_char_re.sub("\g<1> \g<2>", word)
    return word

def contains_number_in_korean(word):
    word = encode(word)
    regex = re.compile("^[가-힣]{1}[0-9]{1}")

    if len(regex.findall(word)) == 0:
        regex = re.compile("^[0-9]{1}[가-힣]{1}")

    return len(regex.findall(word)) > 0


def tokenize_mixed_word(word):
    word = encode(word)

    model_char_re = re.compile("([가-힣]{1})([a-zA-Z]{1})")
    word = model_char_re.sub("\g<1> \g<2>", word)
    result = ""
    for tok in word.split(' '):
        result += " "
        result += tokenize_mixed_word_reverse(tok)

    return result.strip()

def tokenize_mixed_word2(word):
    word = encode(word)

    model_char_re = re.compile("([가-힣]{1})([0-9]{1})")
    word = model_char_re.sub("\g<1> \g<2>", word)

    return word.strip()

def tokenize_dashed_word(word):
    word = encode(word)
    reg = re.compile('([\w\s]*)-([\s가-힣]+)')

    if len(reg.findall(word)) == 0:
        reg = re.compile('([가-힣\s]+)-([\s\w]*)')
        if len(reg.findall(word)) == 0:
            reg = re.compile('([가-힣\s]+)-([\s가-힣]+)')

    return reg.sub('\g<1> - \g<2>', word)

def tokenize_unit(word):
    word = encode(word)

    # TODO : brand 체크..
    if is_seller(word): return word

    regex = re.compile('^([0-9]+\.?[0-9]*)([a-zA-Z가-힣]+)$')
    # print regex.findall(word)
    # matched = regex.match(word)
    # if matched:
    #     print word,matched.groups()
    return regex.sub('\g<1> \g<2>', word)

def is_multiple_unit(word):
    word = encode(word)
    regex = re.compile('^[xX\*]{1}[0-9]+[a-zA-Z가-힣]*$')
    return regex.match(word) is not None

def tokenize_multiple_unit(word):
    word = encode(word)
    items = re.split(r'^([xX\*]{1})', word.strip())
    items[2] = tokenize_unit(items[2])
    return ' '.join(items).strip()

# TODO : × 이문자 포함.. 인코딩 에러발생함
def tokenize_size_spec(txt):
    txt = encode(txt)
    # regex = re.compile('[0-9]+\.?[0-9]*[\w가-힣]*[xX\*]{1}')
    regex = re.compile('^[0-9]+\.?[0-9]*[\w가-힣]*[xX\*]{1}[0-9]+\.?[0-9]*[\w가-힣]*[xX\*]*[0-9]*\.?[0-9]*$')

    for item in regex.findall(txt):
        size_toks = re.split(r'[xX\*]', item)
        join_toks = re.findall(r'[xX\*]', item)
        newitem = []
        for i in range(len(size_toks)):
            newitem.append(size_toks[i])
            if i < len(join_toks):
                newitem.append(join_toks[i])
        txt = txt.replace(item, ' '.join(newitem))

    newtxt = txt
    items = txt.split(' ')
    if len(items) > 1:
        newtxt = ''
        for item in items:
            newtxt += tokenize_unit(item)
            newtxt += " "
        newtxt = newtxt.strip()


    return newtxt

def remove_article(txt):
    regex = re.compile('\s*(a|the|to){1}\s+')
    return regex.sub(' ', txt).strip()

def tokenize_percent(txt):
    txt = encode(txt)
    return re.sub(r'([0-9]+\.?[0-9]*)(\%)', ' \g<1> \g<2> ', txt)

# print tokenize_percent('현대카드40%할인')
# print tokenize_unit("1족")
# print tokenize_unit("10번의결정적순간")
# print tokenize_unit("리필수세미52개")
# print tokenize_unit("12호")
# print tokenize_unit("131-63-10-020")
# print tokenize_unit("5216ML3045")
# print tokenize_unit("9kg화미고맥당")
# print tokenize_unit("24입*2")
# print tokenize_unit("벙커침대40T라텍스독립매트")
# print tokenize_unit("20종")
# print tokenize_unit("60입골라담기")

# print(tokenize_name("가나/다/라"))
# print is_model_name('가나다T61KKT100')
# print is_model_name('T61KKT100')

# model_char_re = re.compile("[0-9a-zA-Z\-]")
# print model_char_re.findall('T61KKT100')
# match = model_char_re.match('가나다T61KKT100')
# print match.string
#
# search = model_char_re.search('T61KKT100')

# print replace_symbols('(맛나)박스/절단낙지(L 인도네시아)600g*6')
# print replace_symbols('Way to Go! 5')

# print " test test ".strip()

# print Hangul.ishangul("안녕".decode('utf8'))
# print Hangul.split("강".decode('utf8'))

# print normalize_number('안녕하세요 마틴85 8.5')
# print replace_specific_token('2015 F/W 내츄럴 니트 넥타이')
# print is_english_word('test')

# print tokenize_mixed_word('QR인증')
# print tokenize_mixed_word('인증QR')
# print tokenize_mixed_word('안녕martin안녕')
# print tokenize_mixed_word('martin안녕tt마틴aa마틴')
# print tokenize_mixed_word('안녕하세요')
# print tokenize_mixed_word('신한카드7%')
# print tokenize_mixed_word('ijs700-치맥이')

# print tokenize_dashed_word('ijs700-치맥이')
# print tokenize_dashed_word('치맥이-ijs700')
# print tokenize_dashed_word('ijs700-DEF')
# print tokenize_dashed_word('텐바이텐 PP 무지핸들박스 소 -반투명 베이킹 빼빼로 포장지')
# print tokenize_dashed_word('아트박스  세비앙  가로본능 up-타올행거')
# print tokenize_dashed_word('꾸밈 iw232-초크아트 푸드 노프레임 무료배송')

# print is_number('1가나다')
# print is_number('.5')
# print is_number('1.5')
# print is_number('15')
# print is_model_name('W-TAM16-151-BUX')
# print is_model_name('adidas')

# print replace_specific_token("[롯데카드7%][구찌][구찌] 명품선글라스 2014년 모델 GUCCI GG 3663/F/S 0WQD8 [GG3663/F/S]")
# print tokenize_size_spec('1.6mx9.0m')
# print tokenize_size_spec('상도가구 300x400x500팩 무료배송 가이아 3x5 회의용 1.6mx9.0m 테이블 1500x900')

# print tokenize_unit("500팩")
# print tokenize_unit("500cm")

# print remove_article('i am the king')
# print remove_article('i am a king')
# print remove_article('a boy')
# print remove_article('the boy')

# print contains_number_in_korean('2종세트')
# print tokenize_size_spec('50×50')
# print contains_number_in_korean('7부팬츠')
# print contains_number_in_korean('10개')
# print is_number('x10봉')
# print is_model_name('x10봉')
# print is_unit('x10봉')
# print is_english_word('x10봉')
# print contains_number_in_korean('x10봉')
# print is_multiple_unit('x10봉')
# print tokenize_multiple_unit('x10봉')
# print tokenize_multiple_unit('X10')
# print tokenize_multiple_unit('X10')
# print is_number('2015년')

# print is_unit('2015년')
# print is_unit('2015세트')
# print is_unit('2015cm')

# print tokenize_mixed_word2('메탈선반750')
# print replace_symbols("간편난방ㆍ당일배송ㆍ설치비별도ㆍ냉*난방기전문ㆍ")
# print replace_symbols("┃히터┃전기히터┃전기난로┃난방기┃원적외선┃온풍기┃전기온풍기┃함마톤┃기능성온풍기")
# print is_multiple_unit('MP3')
