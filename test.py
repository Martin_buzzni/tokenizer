#-*- coding: utf-8 -*-
from sklearn.externals import joblib
import codecs

# def convert_dump2txt(x, y, foutpath):
#     with codecs.open(foutpath, 'w') as fout:
#
#         for i in range(len(x)):
#             item = x[i]
#             label = y[i]
#             if type(item) == unicode:
#                 item = item.encode('utf8')
#             if type(label) == unicode:
#                 label = label.encode('utf8')
#             fout.write("(" + label + ") "+ item + "\n")
#
# x = joblib.load('/Users/martin/workspace/tokenizer/eval/data/justin_x_train.dump')
# y = joblib.load('/Users/martin/workspace/tokenizer/eval/data/justin_y_train.dump')
# convert_dump2txt(x,y, '/Users/martin/workspace/tokenizer/eval/data/justin_x_train.txt')
#
# x = joblib.load('/Users/martin/workspace/tokenizer/eval/data/justin_x_train.twitter.dump')
# y = joblib.load('/Users/martin/workspace/tokenizer/eval/data/justin_y_train.twitter.dump')
# convert_dump2txt(x,y, '/Users/martin/workspace/tokenizer/eval/data/justin_x_train.twitter.txt')

# def merge_tr_dicts(dic_list):
#     result = {}
#     for dic_path in dic_list:
#         print dic_path
#         dic = joblib.load(dic_path)
#         result.update(dic)
#     return result
#
# def merge_tr_dicts2(dic_list):
#     result = {}
#     for dic_path in dic_list:
#         print dic_path
#         dic = joblib.load(dic_path)
#         for k,v in dic.items():
#             if k in result:
#                 result[k].append(v)
#             else:
#                 result[k] = v
#
#     return result
#
# dic_list = []
# dic_list.append('/Users/martin/workspace/tokenizer/dic/backup/to_ko_dic1000.pkl')
# dic_list.append('/Users/martin/workspace/tokenizer/dic/backup/to_ko_dic_dev.pkl')
# dic_list.append('/Users/martin/workspace/tokenizer/dic/backup/to_ko_dic_dev_test.pkl')
# dic_list.append('/Users/martin/workspace/tokenizer/dic/backup/to_ko_dic_justin_test.pkl')
# dic_list.append('/Users/martin/workspace/tokenizer/dic/backup/to_ko_dic_naver.pkl')
# dic_list.append('/Users/martin/workspace/tokenizer/dic/backup/to_ko_dic_test.pkl')
#
# dic = merge_tr_dicts2(dic_list)
# joblib.dump(dic, '/Users/martin/workspace/tokenizer/dic/to_ko_test.dict')

from konlpy.tag import Twitter
from konlpy.tag import Kkma
twitter = Twitter()
kkma = Kkma()

print " ".join(twitter.morphs("런닝화".decode('utf8')))
print " ".join(twitter.morphs("정장화".decode('utf8')))
print " ".join(twitter.morphs("탁상용".decode('utf8')))
print " ".join(twitter.morphs("상하좌우".decode('utf8')))
print " ".join(twitter.morphs("탁상용블로워팬".decode('utf8')))

from datalabs.ryan.buzzni.apis.nlp_api import segment as regacy_segment
print regacy_segment("런닝화")
print regacy_segment("정장화")
print regacy_segment("탁상용")
print regacy_segment("상하좌우")
print regacy_segment("탁상용블로워팬")
print regacy_segment("아기고양이")

print " ".join(kkma.morphs("런닝화".decode('utf8')))
print " ".join(kkma.morphs("정장화".decode('utf8')))
print " ".join(kkma.morphs("탁상용".decode('utf8')))
print " ".join(kkma.morphs("상하좌우".decode('utf8')))
print " ".join(kkma.morphs("탁상용블로워팬".decode('utf8')))